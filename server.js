"use strict";

// Importing modules used by the server
const express = require("express");
const bodyParser = require("body-parser");
const request = require("request");
const jsonParser = bodyParser.json();

// Initializing the server
let app = express();

app.all("*", (req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "POST, OPTIONS");
    res.header("Access-Control-Allow-Headers", "Content-Type");
    next();
});

// Listening post requests on the route localhost:3002/routes
app.post("/routes", jsonParser, (req, res) => {
    // Returning an error if any body is provided in request
    if (!req.body) {
        return res.status(400).send("Bad Request: no body provided");
    }

    // Getting clients
    const clients = req.body.clients;

    // Returning an error if less than two clients are given in the request body
    if (!clients || !Array.isArray(clients) || clients.length < 2) {
        return res.status(400).send("Bad Request: at least two clients expected");
    }

    let url = "http://router.project-osrm.org/trip/v1/driving/";
    const params = ["roundtrip=true", "source=first", "geometries=geojson"];

    // TODO(unknown) update url by passing client coordinates
    // url should be http://router.project-osrm.org/trip/v1/driving/lng_1,lng_2;...;lng_n,lat_n
    for (let client in clients) {

    }

    request
        .get(url + "?" + params.join('&'))
        .on("response", (response) => {
            if (response && response.statusCode === 200) {
                res.send(response);
            }
            else {
                return res.sendStatus(400);
            }
        })
        .on("error", (error) => {
            return res.status(400).send(error);
        });
});

// Initializing port to listen to
app.listen(3002, () => {
    console.log("App listening on port 3002");
});